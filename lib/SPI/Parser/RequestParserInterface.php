<?php

namespace kamilmusial\NasaApi\SPI\Parser;

use kamilmusial\NasaApi\SPI\Persistence\ValueObject;

interface RequestParserInterface
{
    /**
     * @param string $body
     *
     * @return ValueObject[]
     */
    public function parseRequestBody(string $body): array;
}
