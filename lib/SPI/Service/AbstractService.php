<?php

namespace kamilmusial\NasaApi\SPI\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractService
{
    /** @var Registry  */
    protected $doctrine;

    /** @var EntityManagerInterface  */
    protected $entityManager;

    /** @var ObjectRepository  */
    protected $entity;

    /**
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->entityManager = $doctrine->getManager();
    }

    /**
     * @param int|null $id
     *
     * @return mixed
     */
    public function get($id = null)
    {
        return (null === $id) ? $this->getAll() : $this->get($id);
    }

    /**
     * @param int $id
     *
     * @return object
     */
    protected function find(int $id): object
    {
        return $this->entity->find($id);
    }

    /**
     * @return array
     */
    protected function findAll(): array
    {
        return $this->entity->findAll();
    }

    abstract function setEntity();
}
