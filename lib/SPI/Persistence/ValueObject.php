<?php

namespace kamilmusial\NasaApi\SPI\Persistence;

use kamilmusial\NasaApi\Core\Exception\PropertyNotFoundException;
use kamilmusial\NasaApi\Core\Exception\PropertyReadOnlyException;

abstract class ValueObject
{
    /**
     * @param array $properties
     */
    public function __construct(array $properties = [])
    {
        foreach ($properties as $property => $value) {
            $this->$property = $value;
        }
    }

    /**
     * @param string $property
     * @param mixed $value
     *
     * @throws PropertyNotFoundException
     * @throws PropertyReadOnlyException
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            throw new PropertyReadOnlyException($property, get_class($this));
        }
        throw new PropertyNotFoundException($property, get_class($this));
    }

    /**
     * @param string $property
     *
     * @return mixed
     *
     * @throws PropertyNotFoundException
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        throw new PropertyNotFoundException($property, get_class($this));
    }

    /**
     * @param string $property
     *
     * @return bool
     */
    public function __isset($property)
    {
        return property_exists($this, $property);
    }

    /**
     * @param string $property
     */
    public function __unset($property)
    {
        $this->__set($property, null);
    }

    /**
     * @return array
     */
    protected function getProperties()
    {
        $properties = [];

        foreach (get_object_vars($this) as $property => $propertyValue) {
            if ($this->__isset($property)) {
                $properties[] = $property;
            }
        }

        return $properties;
    }
}
