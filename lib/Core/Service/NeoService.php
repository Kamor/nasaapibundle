<?php

namespace kamilmusial\NasaApi\Core\Service;

use Doctrine\ORM\Query\ResultSetMappingBuilder;
use kamilmusial\NasaApi\SPI\Service\AbstractService;
use kamilmusial\NasaApiBundle\Entity\Neo as NeoEntity;
use kamilmusial\NasaApi\Core\Persistence\ValueObject\NEO;

class NeoService extends AbstractService
{
    public function setEntity()
    {
        $this->entity = $this->doctrine->getRepository('kamilmusialNasaApiBundle:Neo');
    }

    /**
     * @param NEO $neo
     *
     * @return int
     */
    public function add(NEO $neo): int
    {
        if ($this->exixts($neo->reference)) {
            return 0;
        }

        $entity = new NeoEntity();

        $entity
            ->setName($neo->name)
            ->setDate($neo->date)
            ->setHazardous($neo->isHazardous)
            ->setReference($neo->reference)
            ->setSpeed($neo->speed)
        ;

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity->getId();
    }

    /**
     * @param NEO[] $neos
     *
     * @return array
     */
    public function bulkAdd(array $neos): array
    {
        $ids =  [];
        $existed = 0;

        foreach ($neos as $neo) {
            if ($added = $this->add($neo)) {
                $ids[] = $added;
            } else {
                $existed++;
            }
        }

        return [$ids, $existed];
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     *
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        $entities = $this->entity->findBy($criteria, $orderBy, $limit, $offset);
        $neos = [];

        foreach ($entities as $entity) {
            $neos[] = new NEO([
                'date' => $entity->getDate()->format('Y-m-d'),
                'reference' => $entity->getReference(),
                'name' => $entity->getName(),
                'speed' => $entity->getSpeed(),
                'isHazardous' => $entity->getHazardous(),
            ]);
        }

        return $neos;
    }

    /**
     * @param string $period
     * @param bool $hazardous
     *
     * @return string
     */
    public function findBestPeriod($period, $hazardous = false): string
    {
        $rsm = (new ResultSetMappingBuilder($this->entityManager))->addScalarResult($period, $period);

        $sql = sprintf('
            SELECT %s(n.date) as %s, 
            COUNT(n.id) as count
            FROM neo n
            WHERE n.hazardous = %s  
            GROUP BY %s
            ORDER BY count DESC',
            $period,
            $period,
            (int)$hazardous,
            $period
        );
        $entities = $this->entityManager->createNativeQuery($sql, $rsm);

        return $entities->getSingleResult()[$period];
    }

    /**
     * @param int $reference
     *
     * @return int
     */
    public function exixts($reference): int
    {
        $entities = $this->entity->findBy([
            'reference' => $reference
        ]);

        return count($entities);
    }
}
