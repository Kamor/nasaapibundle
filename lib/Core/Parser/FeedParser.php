<?php

namespace kamilmusial\NasaApi\Core\Parser;

use kamilmusial\NasaApi\Core\Exception\InvalidResponseBodyException;
use kamilmusial\NasaApi\Core\Persistence\ValueObject\NEO;
use kamilmusial\NasaApi\SPI\Parser\RequestParserInterface;

class FeedParser implements RequestParserInterface
{
    /** @var DateTimeParser */
    protected $dateTimeParser;

    /**
     * @param DateTimeParser $dateTimeParser
     */
    public function __construct(DateTimeParser $dateTimeParser)
    {
        $this->dateTimeParser = $dateTimeParser;
    }

    public function parseRequestBody(string $body): array
    {
        $response = json_decode($body);

        if (json_last_error()) {
            throw new InvalidResponseBodyException(json_last_error_msg());
        }

        $neos = [];
        foreach ($response->near_earth_objects as $day) {
            foreach ($day as $neo) {
                $closeApproachData = array_shift($neo->close_approach_data);

                $neos[] = new NEO([
                    'date' => $this->dateTimeParser->setDateTime($closeApproachData->close_approach_date)->getDateTime(),
                    'reference' => $neo->neo_reference_id,
                    'name' => $neo->name,
                    'speed' => $closeApproachData->relative_velocity->kilometers_per_hour,
                    'isHazardous' => $neo->is_potentially_hazardous_asteroid,
                ]);
            }
        }

        return $neos;
    }

}
