<?php

namespace kamilmusial\NasaApi\Core\Parser;

use DateTime;

class DateTimeParser
{
    /** @var DateTime */
    protected  $dateTime;

    /** @var string */
    protected $format;

    /**
     * @param string $format
     */
    public function __construct(string $format = 'U')
    {
        $this->format = $format;
    }

    /**
     * @param $dateTime
     *
     * @return DateTimeParser
     */
    public function setDateTime($dateTime): self
    {
        if (!$dateTime instanceof DateTime) {
            $dateTime = new DateTime($dateTime);
        }

        $this->dateTime = $dateTime;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateTime(): DateTime
    {
        return $this->dateTime;
    }

    /**
     * @return string
     */
    public function __toString():string
    {
        return $this->dateTime->format($this->format);
    }
}
