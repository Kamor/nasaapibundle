<?php

namespace kamilmusial\NasaApi\Core\Http;

use GuzzleHttp\Client;

class Request
{
    /** @var array */
    private $options;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->options = $options;
    }

    /**
     * @param $resource
     * @param array $params
     *
     * @return string
     */
    public function fetch($resource, array $params = []): string
    {
        $params = array_merge(['api_key' => $this->options['api_key']], $params);

        $response = $this->getHttpClient()->request(
            'GET',
            $this->options['base_url'] . $resource,
            [
                'query' => $params
            ]
        );

        return $response->getBody()->getContents();
    }

    /**
     * @return Client
     */
    private function getHttpClient(): Client
    {
        return new Client([
            'base_uri' => $this->options['base_url'],
        ]);
    }
}
