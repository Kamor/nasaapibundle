<?php

namespace kamilmusial\NasaApi\Core\Exception;

use Exception;

class PropertyNotFoundException extends Exception
{
    public function __construct($property, $class)
    {
        parent::__construct(sprintf('Property %s not found in class %s', $property, $class));
    }

}
