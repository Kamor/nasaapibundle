<?php
namespace kamilmusial\NasaApi\Core\Exception;

use Exception;

class PropertyReadOnlyException extends Exception
{
}
