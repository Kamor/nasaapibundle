<?php

namespace kamilmusial\NasaApi\Core\Persistence\ValueObject;

use kamilmusial\NasaApi\SPI\Persistence\ValueObject;

class NEO extends ValueObject
{
    /** @var \DateTime */
    public $date;

    /** @var int */
    public $reference;

    /** @var string */
    public $name;

    /** @var float */
    public $speed;

    /** @var bool */
    public $isHazardous;
}
