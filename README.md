# Nasa API Bundle

## Symfony

Powerful framework, that I'm using on a daily basis. Easy configurable, extandable and delivered with useful components (such as _Console_, _Routing_, _Templating_ etc.)


## Env:

* Symfony Framework 3.3
* PhpStorm
* PHP 7
* MySQL
* Built-in Symfony server
   
##Tasks

* Bundle was divided into two parts:
    * `bundle` for code directly connected to Symfony
    * `lib` for business logic

* 6 unit tests for 4 classes are present.
* API key and endpoint url are stored in parameters and can be easily overwritten by edit `config.yml`:
```
kamilmusial_nasa_api:
   neo_rest_endpoint: 'NEW ENDPOINT'
   api_key: 'API KEY'
```
API KEY value is commited for demo purposes only, I'd be removed from the repo for security reasons.

### 1. Specify a default controller

* `DefaultController` created with `indexAction`.

### 2. Use the api.nasa.gov

* `kamilmusial\NasaApi\Core\Http\Request` class created. 
* It's constructor accepts array of options, which includes `api_key` provided by configuration under `kamilmusial_nasa_api.api_key`.

### 3. Write a command

* `kamilmusial\NasaApiBundle\Command\FetchNeosCommand` created.
* command name is `kmnasa:fetchneo`
* The command depends on prevoiusly mentioned `Request` class, a Parser for response and a Service to store the data.
* Number of last days to fetch is an option with default value of `3`.
* Model of NEO inherits from abstract value object class to be more flexible.
    
### 4. Create a route `/neo/hazardous`

* Route created to fetch neos from the Service.

### 5. Create a route `/neo/fastest?hazardous=(true|false)`

* Route created and handled by an action.

### 6. Create a route `/neo/best-year?hazardous=(true|false)`
### 7. Create a route `/neo/best-month?hazardous=(true|false)`

* Both routes merged into one action.
* Possible periods are set to `year` or `month`

## Next steps

Next step would be:

* create command for fetching more data at once (current API limit is 7 per request)
* provide an option to use different DB engine (to support last two routes)
* write more tests (including different env for persistence testing)
