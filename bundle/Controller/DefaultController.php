<?php

namespace kamilmusial\NasaApiBundle\Controller;

use kamilmusial\NasaApi\Core\Service\NeoService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController
{
    /** @var NeoService */
    protected $neoService;

    /**
     * @param NeoService $neoService
     */
    public function __construct(NeoService $neoService)
    {
        $this->neoService = $neoService;
    }

    /**
     * @return JsonResponse
     */
    public function indexAction(): JsonResponse
    {
        return new JsonResponse(['hello' => 'world!']);
    }

    /**
     * @return JsonResponse
     */
    public function showHazardousAction(): JsonResponse
    {
        $neos = $this->neoService->findBy(['hazardous' => true]);

        return new JsonResponse($neos);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function showFastestAction(Request $request): JsonResponse
    {
        $hazardous = $request->get('hazardous', false);

        $neos = $this->neoService->findBy(
            ['hazardous' => (bool)$hazardous],
            ['speed' => 'DESC'],
            1
        );

        return new JsonResponse($neos);
    }

    /**
     * @param Request $request
     * @param string $period
     *
     * @return JsonResponse
     */
    public function bestPeriodAction(Request $request, string $period): JsonResponse
    {
        $hazardous = $request->get('hazardous', false);

        $bestPeriod = $this->neoService->findBestPeriod($period, $hazardous);

        return new JsonResponse(['best ' . $period => $bestPeriod]);
    }
}
