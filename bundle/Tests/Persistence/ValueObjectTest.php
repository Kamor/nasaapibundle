<?php

namespace kamilmusial\NasaApiBundle\Tests\Persistence;

use kamilmusial\NasaApi\SPI\Persistence\ValueObject;
use PHPUnit\Framework\TestCase;

class ValueObjectTest extends TestCase
{
    /** @var ValueObject */
    protected $objectClass;

    public function setUp()
    {
        $this->objectClass = new class extends ValueObject {
            protected $protectedProperty;
            public $publicProperty;
            public $existing = 'set';
        };

        parent::setUp();
    }

    /**
     * @expectedException \kamilmusial\NasaApi\Core\Exception\PropertyNotFoundException
     */
    public function testPropertyNotFound()
    {

        $object = new $this->objectClass([
            'protectedProperty' => 'test1',
            'publicProperty' => 'test2',
        ]);

        $test = $object->property;
    }
}
