<?php

namespace kamilmusial\NasaApiBundle\Tests\Http;

use kamilmusial\NasaApi\Core\Http\Request;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{
    /** @var \kamilmusial\NasaApi\Core\Http\Request */
    protected $request;

    public function setUp()
    {
        $this->request = new Request(['base_url' => 'http://example.com']);
    }

    public function testGetHttpClient()
    {
        $client = $this->invokeMethod($this->request, 'getHttpClient');
        $this->assertInstanceOf('GuzzleHttp\Client', $client);
    }

    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
