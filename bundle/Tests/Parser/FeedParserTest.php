<?php

namespace kamilmusial\NasaApiBundle\Tests\Parser;

use DateTime;
use kamilmusial\NasaApi\Core\Parser\DateTimeParser;
use kamilmusial\NasaApi\Core\Parser\FeedParser;
use kamilmusial\NasaApi\Core\Persistence\ValueObject\NEO;
use PHPUnit\Framework\TestCase;


class FeedParserTest extends TestCase
{
    /** @var FeedParser */
    protected $feedParser;

    /** @var string */
    protected $feed = '{
        "links": {
            "next": "https://api.nasa.gov/neo/rest/v1/feed?start_date=2017-06-28&end_date=2017-07-01&detailed=true&api_key=N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD",
            "prev": "https://api.nasa.gov/neo/rest/v1/feed?start_date=2017-06-22&end_date=2017-06-25&detailed=true&api_key=N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD",
            "self": "https://api.nasa.gov/neo/rest/v1/feed?start_date=2017-06-25&end_date=2017-06-28&detailed=true&api_key=N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD"
        },
        "element_count": 20,
        "near_earth_objects": {
            "2017-06-28": [
                {
                    "links": {
                        "self": "https://api.nasa.gov/neo/rest/v1/neo/2066253?api_key=N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD"
                    },
                    "neo_reference_id": "2066253",
                    "name": "66253 (1999 GT3)",
                    "nasa_jpl_url": "http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=2066253",
                    "absolute_magnitude_h": 18.3,
                    "estimated_diameter": {
                        "kilometers": {
                            "estimated_diameter_min": 0.5815070396,
                            "estimated_diameter_max": 1.30028927
                        },
                        "meters": {
                            "estimated_diameter_min": 581.5070396458,
                            "estimated_diameter_max": 1300.2892700427
                        },
                        "miles": {
                            "estimated_diameter_min": 0.3613316107,
                            "estimated_diameter_max": 0.807962044
                        },
                        "feet": {
                            "estimated_diameter_min": 1907.8315559515,
                            "estimated_diameter_max": 4266.0410487267
                        }
                    },
                    "is_potentially_hazardous_asteroid": false,
                    "close_approach_data": [
                        {
                            "close_approach_date": "2017-06-28",
                            "epoch_date_close_approach": 1498633200000,
                            "relative_velocity": {
                                "kilometers_per_second": "36.3977864061",
                                "kilometers_per_hour": "131032.031061803",
                                "miles_per_hour": "81418.2084117076"
                            },
                            "miss_distance": {
                                "astronomical": "0.3344177972",
                                "lunar": "130.0885162354",
                                "kilometers": "50028188",
                                "miles": "31086076"
                            },
                            "orbiting_body": "Earth"
                        }
                    ],
                    "orbital_data": {
                        "orbit_id": "37",
                        "orbit_determination_date": "2017-06-12 06:19:36",
                        "orbit_uncertainty": "0",
                        "minimum_orbit_intersection": ".192505",
                        "jupiter_tisserand_invariant": "4.422",
                        "epoch_osculation": "2458000.5",
                        "eccentricity": ".8378333961211405",
                        "semi_major_axis": "1.333940609138991",
                        "inclination": "19.51070229109088",
                        "ascending_node_longitude": "157.5712148770086",
                        "orbital_period": "562.734015695559",
                        "perihelion_distance": ".2163206183601673",
                        "perihelion_argument": "260.8982285712017",
                        "aphelion_distance": "2.451560599917815",
                        "perihelion_time": "2457968.875347469203",
                        "mean_anomaly": "20.23136080909378",
                        "mean_motion": ".6397338528665756",
                        "equinox": "J2000"
                    }
                }
            ]
        }
    }';

    /** @var NEO */
    protected $neo;

    public function setUp()
    {
        $this->feedParser = new FeedParser(new DateTimeParser());
        $this->neo = [new NEO([
            'date' => new DateTime('2017-06-28'),
            'reference' => 2066253,
            'name' => '66253 (1999 GT3)',
            'speed' => 131032.031061803,
            'isHazardous' => false,
        ])];

        parent::setUp();
    }

    public function testParseRequestBody()
    {
        $parserValue = $this->feedParser->parseRequestBody($this->feed);

        $this->assertEquals($this->neo, $parserValue);
    }

    /**
     * @expectedException \kamilmusial\NasaApi\Core\Exception\InvalidResponseBodyException
     * @expectedExceptionMessage Syntax error
     */
    public function testInvalidResponseBody()
    {
        $parserValue = $this->feedParser->parseRequestBody('{{}{}[{}]}{}');
    }
}
