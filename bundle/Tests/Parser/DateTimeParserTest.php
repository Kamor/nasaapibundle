<?php

namespace kamilmusial\NasaApiBundle\Tests\Parser;

use DateTime;
use kamilmusial\NasaApi\Core\Parser\DateTimeParser;
use PHPUnit\Framework\TestCase;

class DateTimeParserTest extends TestCase
{
    /** @var DateTimeParser */
    protected $dateTimeParser;

    public function setUp()
    {
        $this->dateTimeParser = new DateTimeParser();

        parent::setUp();
    }

    public function testSetDateTime()
    {
        $this->dateTimeParser->setDateTime(new DateTime('2017-06-29'));

        $this->assertEquals(new DateTime('2017-06-29'), $this->dateTimeParser->getDateTime());
    }

    public function testSetDateTimeString()
    {
        $this->dateTimeParser->setDateTime('2017-06-29');

        $this->assertEquals(new DateTime('2017-06-29'), $this->dateTimeParser->getDateTime());
    }
}
