<?php

namespace kamilmusial\NasaApiBundle\Command;

use DateTime;
use kamilmusial\NasaApi\Core\Http\Request;
use kamilmusial\NasaApi\Core\Parser\FeedParser;
use kamilmusial\NasaApi\Core\Service\NeoService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FetchNeosCommand extends ContainerAwareCommand
{
    const DEFAULT_DAYS_COUNT = 3;

    /** @var Request */
    protected $request;

    /** @var FeedParser */
    protected $parser;

    /** @var NeoService */
    protected $neoService;


    public function __construct(Request $request, FeedParser $parser, NeoService $neoService)
    {
        $this->request = $request;
        $this->parser = $parser;
        $this->neoService = $neoService;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('kmnasa:fetchneo')
            ->setDescription(sprintf('Fetches NEOs occurences within set days (default: %d)', self::DEFAULT_DAYS_COUNT))
            ->setHelp(<<<EOT
This command fetches latest NEOs occurences from Nasa API. 
You can specify the number of days to fetch by adding option <info>-c</info> <comment>[default: 3]</comment>
EOT
            )
            ->addOption('number', 'c', InputOption::VALUE_OPTIONAL, 'Number of days you want to fetch from today.', 3)
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '<info>Fetching the data.</info>',
            '<comment>------------------</comment>',
            '',
        ]);

        $endDate = new DateTime();
        $startDate = (new DateTime())->modify(sprintf('-%s days', $input->getOption('number')));

        $body = $this->request->fetch('feed', [
            'start_date' => $startDate->format('Y-m-d'),
            'end_date' => $endDate->format('Y-m-d'),
            'detailed' => 'true',
        ]);

        $neos = $this->parser->parseRequestBody($body);
        list($ids, $existed) = $this->neoService->bulkAdd($neos);

        $output->writeln(sprintf('%d NEOs added.', count($ids)));

        if ($existed) {
            $output->writeln(sprintf('%d already existed in database.', $existed));
        }
    }
}
