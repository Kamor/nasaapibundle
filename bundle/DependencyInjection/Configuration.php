<?php

namespace kamilmusial\NasaApiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('kamilmusial_nasa_api');

        $rootNode
            ->children()
                ->scalarNode('neo_rest_endpoint')
                    ->info('URL of the APIs endpoint')
                    ->isRequired()
                ->end()
                ->scalarNode('api_key')
                    ->info('API key')
                    ->isRequired()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
